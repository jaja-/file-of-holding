/*
FILE OF HOLDING
Copyright (C) 2023  Jason Jansen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <limits.h>
#include <string.h>
#include <unistd.h>

#include <sqlfs.h>

#include "log.h"
#include "credential.h"

#warning "TODO: Add create and reset options"
void usage() {
  printf(
    "USAGE:\n"
    "  foh [OPTIONS [PATHS]] [-f FUSE_1 ... N] DB_FILE MNT_FOLDER\n"
    "\n"
    "OPTIONS:\n"
    "  -p         Use a password to encrypt/open a database on mount\n"
    "  -r         Reset a password on a database\n"
    "  -k [PATH]  Use a key to encrypt/open a database on mount\n"
    "  -n [PATH]  Rekey a datbase. Must provide original key with '-k'\n"
    "  -f         End reading FOH flags, read FUSE flags\n"
    "  -h         Print usage information\n"
  ); 
}


int main(int argc, char** argv) {
  int rc;
//   sqlite3* db;
  char dbpath[PATH_MAX];
  int fuse_argc;
  char** fuse_argv;
  bool fuse = false;
  ssize_t fuse_arg_count = 0;
  
  struct credential* password = NULL;
  struct credential* newpassword = NULL;
  struct credential* key = NULL;
  struct credential* newkey = NULL;

  // Parse command line arguments
  uint8_t aidx = 0;
  while(!fuse && aidx+1 < argc && argv[aidx+1][0] == '-') {
    aidx++;
    switch(argv[aidx][1]) {
      case 'r':
        if(!password)
          password = get_password("Password");
        newpassword = get_password("New Password");
        break;
        
      case 'p':
        if(!newpassword)
          password = get_password("Password");
        break;
        
      case 'k':
        key = get_key(argv[aidx + 1]);
        aidx++;
        break;
        
      case 'n':
        newkey = get_key(argv[aidx + 1]);
        aidx++;
        break;
        
      case 'f':
        fuse = true;
        break;
        
      case 'h':
        usage();
        exit(0);
        
      default:
        usage();
        errxit(EINVAL, "Unknown option: %s", argv[aidx]);
    }
  }
  
  // Minimum (3): foh <DB> <MNT>
  if(argc - aidx < 3) {
    usage();
    errxit(EINVAL, "Not enough arguments");
  }

  // Prepare FUSE arguments
  if(fuse)
    fuse_arg_count = argc - aidx - 3;  // Less above args, program, db, and mnt

  fuse_argc = fuse_arg_count + 2;
  fuse_argv = malloc(sizeof(char*) * fuse_argc);
  
  fuse_argv[0] = strdup(argv[0]);  // Get program path
  for(int i = 0; i < fuse_arg_count + 1; i++)
    fuse_argv[i+1] = strdup(argv[i+aidx]);
  fuse_argv[fuse_arg_count+1] = strdup(argv[argc-1]);  // Get mount path
  
  realpath(argv[argc-2], dbpath);   // Get database path
  
  // Process the credentials
// sqlfs_t* sqlfs = 
  process_credentials(dbpath, password, newpassword, key, newkey);
//   db = (sqlite3*)(sqlfs->db);

  // Run FUSE and return when complete
  rc = sqlfs_fuse_main(fuse_argc, fuse_argv);
  sqlfs_destroy();
    
  return rc;
}
