#!/bin/bash

export TEST="${1}"
export LOG="${2}"
export ERR="${LOG}.err"
export TESTDIR="${3}"
export INIT="${4}"

# https://unix.stackexchange.com/a/504829
cleanup() {
  # Test script can define a custom clean-up
  # procedure. Good for removing mounts
  if [[ $(type -t on_error) == function ]]; then
    on_error
  fi

  exec 1>&5 2>&6
  echo -e "\e[1;31mFAILED\e[0m"
  echo "=== Error ${2} - Line: ${1} ===" >> "${LOG}"
  echo "See ${LOG} and ${ERR} for details"
  
  rm -rf "${TESTDIR}"
  exit $?
}
trap 'cleanup $LINENO $?' ERR


echo -n "==> Case "$(basename "${1}" .test)": "

cd "${TESTDIR}"

exec 5>&1 6>&2
exec 1>"${LOG}" 2>"${ERR}"

if [ ! -z "${INIT}" ]; then
  source "${INIT}"
fi

source "${TEST}"

exec 1>&5 2>&6

echo -e "\e[1;32mPASSED\e[0m"
