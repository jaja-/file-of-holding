defines=-D_GNU_SOURCE=1 -DHAVE_LIBSQLCIPHER=1 -DHAVE_LIBFUSE=1 -D_FILE_OFFSET_BITS=64
warnings=-Wall -Wno-unused-result -Wno-format-truncation
libs=-lsqlfs-1.0 -lsqlcipher -lssl -lcrypto
cflags=-O2

CFLAGS=$(cflags) $(warnings) $(defines)
LDFLAGS=$(libs)

SRCS=$(wildcard *.c)
HDRS=$(patsubst %.c,%.h,$(SRCS))
OBJS=$(patsubst %.c,%.o,$(SRCS))

.PHONY: all
all: foh
	strip $^

.PHONY: clean
clean:
	rm -f foh *.h *.o *.gch
	$(MAKE) -C test clean

	
$(HDRS): %.h: %.c
	ann -g $<
	
	
$(OBJS): %.o: %.c
	$(CC) $(CFLAGS) -c $<

	
foh: $(HDRS) $(OBJS)
	$(CC) $(CFLAGS) -o $@ $< *.o $(LDFLAGS)

	
.PHONY: test
test: foh
	$(MAKE) -C test test

	
.PHONY: bar
bar: clean all test
