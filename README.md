```
 ___ ___ ___ 
|   |   |   |  _______ _____ _       _______     _____  _______   
|___|___|___| (_______|_____) |     (_______)   / ___ \(_______)  
|   |   |   |  _____     _  | |      _____     | |   | |_____     
|___|___|___| |  ___)   | | | |     |  ___)    | |   | |  ___)    
|   |   |     | |      _| |_| |_____| |_____   | |___| | |        
|___|___|     |_|     (_____)_______)_______)   \_____/|_|                                                                                                                 
     ___ ___   _     _  _____  _       _____ _____ ______   ______ 
    |   |   | | |   | |/ ___ \| |     (____ (_____)  ___ \ / _____)     
 ___|___|___| | |__ | | |   | | |      _   \ \ _  | |   | | /  ___ 
|   |   |   | |  __)| | |   | | |     | |   | | | | |   | | | (___)             
|___|___|___| | |   | | |___| | |_____| |__/ /| |_| |   | | \____/|
|   |   |   | |_|   |_|\_____/|_______)_____(_____)_|   |_|\_____/
|___|___|___| 
```

```sh
USAGE:
  foh [OPTIONS [PATHS]] [-f FUSE_1 ... N] DB_FILE MNT_FOLDER

OPTIONS:
  -p         Use a password to encrypt/open a database on mount
  -r         Reset a password on a database
  -k [PATH]  Use a key to encrypt/open a database on mount
  -n [PATH]  Rekey a datbase. Must provide original key with '-k'
  -f         End reading FOH flags, read FUSE flags
  -h         Print usage information
  
DB_FILE:
  Path to the database file storing the contents. If the database
  does not exist, it will be created. If the database exists, the
  following rules apply:
  
  - A database created with a password, must use that password
  - A database created with a key, must use that key
  - A database created without either, cannot obtain either
  
MNT_FOLDER:
  Folder to mount the FUSE filesystem to.
```
---
### Introduction
Inspired by the 'Bag of Holding' from Dungeons and Dragons, I wanted
a way to store files within a single, underlying file. This would
make organizing and transporting said files easier, especially for
archives. In addition, I wanted to add a way to encrypt the
data, so that the files written to this collecive file would be
able to contain more private or critical information safely. My
original approach was to use an image file and LUKS to achieve
this. The process was as such:

1. Use `truncate` to create a blank image file
2. Use a loopback to mount the new image file to a loop
3. With `fdisk`, create a new DOS table and one partition
4. Format the new partition with EXT4
5. Set up the new partition with LUKS using `cryptsetup`
6. Map the encrypted partition with `cryptsetup`
7. Mount the mapped partition to the target folder

The approach used here is more user-friendly (does not
require root or setuid permissions) and simply wraps
an existing library (to make using the library more
accessible).

---
### Dependencies
The following libraries are required in order to use this program:
- ANN: [ann](https://gitlab.com/jaja-/ann)
- FUSE: [libfuse.so.2](https://github.com/libfuse/libfuse)
- SQLFS: [libsqlfs-1.0.so.1](https://github.com/guardianproject/libsqlfs)
- SQl Cipher: [libsqlcipher.so.0](https://github.com/sqlcipher/sqlcipher)
- OpenSSL: [libssl.so](https://github.com/openssl/openssl)
- (Optional) Sqlite ZSTD Plugin: [libsqlite_zstd.so](https://github.com/phiresky/sqlite-zstd)

Some Notes:
- The main dependencies for FOH are FUSE and SQLFS. SQL Cipher and
subsequently, OpenSSL, are used in SQLFS for encryption. They are
optional when building SQLFS.
- ~~Compliling the latest versions of SQL Cipher seemed to leave out
a couple of necessary functions for linking (namely, "sqlite3_key").
You may need to use a different pre-compiled library that contains
that reference.~~  
An extra define is required to make this work:  
`-DSQLITE_HAS_CODEC`  
[See here](https://www.zetetic.net/sqlcipher/design/) for more information.

### Extra Information
[ASCII title credit](https://patorjk.com/software/taag/#p=display&f=Stop&t=FILE%20OF%20HOLDING)

### Future Development:
- Test scripts
- AppImage option
