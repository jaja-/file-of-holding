#include "credential.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <openssl/sha.h>


/*\

#include <stdint.h>

#include <sqlite3.h>
#include <sqlfs.h>

// Necessary for getting the database from the sqlfs_t
// Pulled from sqlfs.c in SQLFS repository
struct sqlfs_t
{
    sqlite3* db;
    int transaction_level;
    int in_transaction;
    mode_t default_mode;
    
    sqlite3_stmt *stmts[200];
#ifndef HAVE_LIBFUSE
    uid_t uid;
    gid_t gid;
#endif
};


// Struct to store key/password information and its length
// Max size for key/password is 512 (as defined in SQLFS)
struct credential {
  char data[MAX_PASSWORD_LENGTH];
  ssize_t length;
};


struct credential* get_password(char* prompt);
struct credential* get_key(char* path);
void clear_credentials(struct credential* input);

sqlfs_t* process_credentials(
  char* dbpath,
  struct credential* password,
  struct credential* newpassword,
  struct credential* key,
  struct credential* newkey
);

\*/

/*
 * Reads a password from the command line
 * char* prompt: Prompt to print to the user
 */
struct credential* get_password(char* prompt) {
  struct credential* ret = calloc(sizeof(struct credential), 1);
  
  char* pass_prompt = malloc(strlen(prompt) + 3);
  snprintf(pass_prompt, strlen(prompt) + 3, "%s: ", prompt);
  
  char* input = getpass(pass_prompt);
  snprintf(ret->data, MAX_PASSWORD_LENGTH, "%s", input);
  
  free(pass_prompt);
  free(input);
  
  return ret;
}


/*
 * Reads a provided key file
 * char* path: Path to the key file to read
 */
struct credential* get_key(char* path) {
  struct credential* ret = calloc(sizeof(struct credential), 1);
  
  ssize_t datalen;
  uint8_t data[MAX_PASSWORD_LENGTH];
  uint8_t hash[SHA256_DIGEST_LENGTH];
  
  SHA256_CTX sha256;
  SHA256_Init(&sha256);

  FILE* fp = fopen(path, "rb");
  if(fp == NULL)
    perrxit("Error opening keyfile %s\n", path);
  
  datalen = fread(data, 1, MAX_PASSWORD_LENGTH, fp);
  if(ret->length < 0)
    perrxit("Error reading keyfile %s\n", path);
  
  fclose(fp);
  
  SHA256_Update(&sha256, data, datalen);
  SHA256_Final(hash, &sha256);
  
  memcpy(ret->data, hash, REQUIRED_KEY_LENGTH);
  ret->length = REQUIRED_KEY_LENGTH;
  memset(data, 0, datalen);
  
  return ret;
}


/*
 * Zeros and frees the credential's memory
 * struct credential* input: Credential to free
 */
void clear_credentials(struct credential* input) {
  if(input)
    memset(input->data, 0, input->length);
  free(input);
}


/*
 * Process the input creditials and setup the SQLFS object
 * char* dbpath: Path to the database
 * struct credential* password: Captured pasword from user
 * struct credential* newpassword: Captured new password from user
 * struct credential* key: Key data from file
 * struct credential* newkey: New key data from file
 */
sqlfs_t* process_credentials(
  char* dbpath,
  struct credential* password,
  struct credential* newpassword,
  struct credential* key,
  struct credential* newkey
) {
  
  int rc;
  sqlfs_t* ret;
  
  // Ensure key and password not both defined
  if(password && key)
    errxit(EINVAL, "Cannot use password and keyfile");
  
  // Ensure old and new keys provided for rekey
  if(newkey && !key)
    errxit(EINVAL, "Cannot rekey without providing the original key");

  // Silence SQLITE3 prints
  FILE* err = pause_stderr();
  
  // Handle password reset
  if(password && newpassword) {
    if(rc = sqlfs_change_password(dbpath, password->data, newpassword->data), !rc)
      errxit(rc, "Error updating database password");
    else
      errxit(0, "Database password updated successfuly");
  }
  
//   int errfd = pause_stderr();`
  
  // Handle key reset
  if(key && newkey) {
    if(rc = sqlfs_rekey(dbpath, (uint8_t*)key->data, key->length, (uint8_t*)newkey->data, newkey->length), !rc)
        errxit(rc, "Error updating database key");
      else
        errxit(0, "Database key updated successfuly");
  }
  
  // If password provided
  if(password) {
    if(rc = sqlfs_open_password(dbpath, password->data, &ret), !rc)
      errxit(1, "Error opening database with password");
    
    sqlfs_init_password(dbpath, password->data);
    
  // If key provided
  } else if(key) {    
    if(rc = sqlfs_open_key(dbpath, (uint8_t*)key->data, key->length, &ret), !rc)
      errxit(2, "Error opening database with key");
    
    sqlfs_init_key(dbpath, (uint8_t*)key->data, key->length);
    
  // If not encypted
  } else {
    if(rc = sqlfs_open(dbpath, &ret), !rc)
      errxit(3, "Error opening database");
    
    sqlfs_init(dbpath);
  }
  
  // Un-Silence SQLITE3 prints
  resume_stderr(err);
  
  clear_credentials(password);
  clear_credentials(newpassword);
  clear_credentials(key);
  clear_credentials(newkey);

  return ret;
}



  
