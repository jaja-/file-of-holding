#include "log.h"

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

/*\
#include <stdio.h>

void errxit(int err, char* fmt, ...);
void perrxit(char* fmt, ...);
FILE* pause_stderr();
void resume_stderr(FILE* err);

\*/

/*
 * Print an error message and exit with provided exit code (not newline necessary)
 * int err: Error code to exit with
 * char* fmt: Printf style output (to stderr)
 */
void errxit(int err, char* fmt, ...) {
  va_list vl;
  
  va_start(vl, fmt);
  vfprintf(stderr, fmt, vl);
  va_end(vl);
  
  fprintf(stdout, "\n");
  
  exit(err);
}


/*
 * Print an error message, exit with errno, and print errno meaning (not newline necessary)
 * char* fmt: Printf style output (to stderr)
 */
void perrxit(char* fmt, ...) {
  va_list vl;
  
  va_start(vl, fmt);
  vfprintf(stderr, fmt, vl);
  va_end(vl);
  
  fprintf(stdout, "\nERRNO %i: %s\n", errno, strerror(errno));
  
  exit(errno);
}


/*
 * Prevents code from printing to STDERR. Using this to supress
 * SQLite3 error output (which is otherwise handled)
 */
FILE* pause_stderr() {
  FILE* err = stderr;
  stderr = fopen("/dev/null", "w");
  return err;
}


/*
 * Resumes allowing code to to print to STDERR
 */
void resume_stderr(FILE* err) {
  fclose(stderr);
  stderr = err;
}
